﻿using System.Web.Optimization;

namespace _383.Angular.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            /*
             
             * Create a bundle for our app's JavaScript
             * which will automatically include any js files
             * within the ~/Assets/app/ directory
             
             */

            var appBundle = new ScriptBundle("~/bundles/app");

            appBundle
                .Include("~/Assets/app/module.js") //explicitly include the file which creates our angular module first
                .IncludeDirectory("~/Assets/app", "*.js");

            bundles.Add(appBundle);
        }
    }
}
