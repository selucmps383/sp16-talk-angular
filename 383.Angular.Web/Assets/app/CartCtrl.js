(function() {
	'use strict';

	angular
		.module('shop')
		.controller('CartCtrl', ['itemService', function(itemService) {
			var cartCtrl = this;

			cartCtrl.items = itemService.cart;
			cartCtrl.getTotal = function() {
			    return _.reduce(cartCtrl.items, function(total, i) {
			        return total + i.price;
			    }, 0.00);
			}

		}]);

})();