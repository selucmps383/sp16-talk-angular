(function() {
	'use strict';

	angular
		.module('shop')
		.controller('ListCtrl', ListCtrl);

    ListCtrl.$inject = ['$http', 'itemService'];

	function ListCtrl($http, itemService) {
	    var listCtrl = this;

	    listCtrl.currentItem = itemService.current;
	    listCtrl.addToCart = itemService.addToCart;
	    listCtrl.selectItem = itemService.selectItem;
	    listCtrl.clearSelection = itemService.clearSelection;

	    $http
            .get('https://api.myjson.com/bins/154md')
            .then(function(response) {
                listCtrl.items = response.data;
            });
	}


})();