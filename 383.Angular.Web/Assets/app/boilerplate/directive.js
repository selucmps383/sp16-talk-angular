(function() {
	'use strict';

	angular
		.module('shop')
		.directive('myDirective', myDirective);

	myDirective.$inject = [];

	function myDirective() {
		return {
			restrict: 'A',
			scope: {},
			templateUrl: '',
			link: function(scope, element, attr) {
				
			}
		}
	}

})();