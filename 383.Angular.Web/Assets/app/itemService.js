(function() {
	'use strict';

	angular
		.module('shop')
		.factory('itemService', itemService);

	itemService.$inject = ['$http'];

	function itemService($http) {
		var service = {
			current: {},
			cart: [],
			selectItem: selectItem,
            clearSelection: clearSelection,
			addToCart: addToCart,
		};

		function selectItem(item) {
		    //should query your API using the passed in item as a parameter

			$http
				.get('https://baconipsum.com/api/?type=all-meat&paras=1') 
				.then(function (response) {
                    angular.copy(item, service.current);
                    service.current.details = response.data[0];
				});	
		}

        function clearSelection() {
            angular.copy({}, service.current);
        }

		function addToCart(item) {
			var copy = angular.copy(item);
			service.cart.push(copy);
		}

		return service;
	}


})();