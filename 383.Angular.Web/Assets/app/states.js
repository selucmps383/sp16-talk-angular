(function() {
	'use strict';

	angular
		.module('shop')
		.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
			$urlRouterProvider.otherwise('/browse');

			$stateProvider
				.state('browse', {
					url: '/browse',
					views: {
						list: {
							templateUrl: './assets/views/list.html'
						},
						cart: {
						    templateUrl: './assets/views/cart.html'
						}
					}
				});
		}]);

})();