﻿using System.Web.Mvc;

namespace _383.Angular.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}